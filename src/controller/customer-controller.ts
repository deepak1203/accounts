import services from "../services/customer-services";
import { Request, Response } from "express";
import message from "../message";

const getCutomers = async (_req: Request, res: Response) => {
  try {
    const result = await services.getAll();
    res.send(result);
  } catch (error) {
    res.send(`${error}`);
  }
};

const addCustomers = async (req: Request, res: Response) => {
  try {
    const {
      firstName,
      lastName,
      address1,
      address2,
      city,
      state,
      zip,
      country,
    } = req.body;
    const result = await services.create(
      firstName,
      lastName,
      address1,
      address2,
      city,
      state,
      zip,
      country
    );
    res.json({ message: message.createPost, customer: result });
  } catch (error) {
    res.send(`${error}`);
  }
};

const deleteCustomer = async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const result = await services.deleteCustomer(id);
    res.json({ message: `message.deletePost`, result });
  } catch (error) {
    res.send(`${error}`);
  }
};

export default {
  getCutomers,
  addCustomers,
  deleteCustomer,
};
