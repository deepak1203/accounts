import { Request, Response } from "express";
import { register, login } from "../services/admin-services";
import bcrypt from "bcryptjs";
import { Admin } from "../model/admin-model";
import message from "../message";

export default class AdminController {
  async register(req: Request, res: Response) {
    try {
      const { fullName, email, password } = req.body;
      const encryptedPassword = await bcrypt.hash(password, 12);
      await register(fullName, email, encryptedPassword);
      res.send(`Hi ${req.body.fullName} you are successfully registerd...😊`);
    } catch (e) {
      res.send(`${e}`);
    }
  }

  async logIn(req: Request, res: Response) {
    try {
      const { email, password } = req.body;
      const admin = await Admin.findOne({ where: { email: email } });
      if (!admin) {
        res.status(401).send(message.wrong);
      }
      const validPass = await bcrypt.compare(
        password,
        admin?.getDataValue("password")
      );
      if (!validPass) {
        res.status(401).send(new Error(message.invalid));
      }
      const token = login(admin);
      res.send({ token, message: message.welcome });
    } catch (e) {
      res.send(`${e}`);
    }
  }
}
