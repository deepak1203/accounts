import { Request, Response } from "express";
import { register, login } from "../services/user-services";
import bcrypt from "bcryptjs";
import { User } from "../model/user-model";
import message from "../message";

export default class UserController {
  async register(req: Request, res: Response) {
    try {
      const { firstName, lastName, email, password } = req.body;
      console.log(firstName, lastName, email);
      const encryptedPassword = await bcrypt.hash(password, 12);
      await register(firstName, lastName, email, encryptedPassword);
      res.send(`Hi ${req.body.firstName} you are successfully registerd...😊`);
    } catch (e) {
      res.send(`${e}`);
    }
  }

  async logIn(req: Request, res: Response) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ where: { email: email } });
      if (!user) {
        res.status(401).send(message.wrong);
      }
      const validPass = await bcrypt.compare(
        password,
        user?.getDataValue("password")
      );
      if (!validPass) {
        res.status(401).send(new Error(message.invalid));
      }
      const token = login(user);
      res.send({ token, message: message.welcome });
    } catch (e) {
      res.send(`${e}`);
    }
  }
}
