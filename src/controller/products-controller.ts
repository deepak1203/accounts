import services from "../services/products-services";
import { Request, Response } from "express";
import message from "../message";

const getProducts = async (_req: Request, res: Response) => {
  try {
    const result = await services.getAll();
    res.send(result);
  } catch (error) {
    res.json({ message: `${error}` });
  }
};

const getById = async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const userID = res.locals.userID;
    const check = await services.getById(userID);
    if (check === null) throw new Error(message.authError);
    const result = await services.getById(id);
    if (result === null) throw new Error(message.notFound);
    res.json(result);
  } catch (error) {
    res.json({ message: `${error}` });
  }
};

const createProduct = async (req: Request, res: Response) => {
  try {
    const { title, details, price, productImage } = req.body;
    const result = await services.create(title, details, productImage, price);
    res.json({ message: message.createPost, product: result });
  } catch (error) {
    res.json({ message: `${error}` });
  }
};

const updateProduct = async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const { title, details, price } = req.body;
    const checkPost = await services.getById(id);
    if (checkPost === null) throw new Error(message.notFound);
    await services.update(id, title, details, price);
    res.json({
      message: message.updatePost,
    });
  } catch (error) {
    res.json({ message: `${error}` });
  }
};

const deleteProduct = async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    await services.deleteBlog(id);
    res.json({ message: `message.deletePost` });
  } catch (error) {
    res.json({ message: `${error}` });
  }
};

const bulkDltProduct = async (req: Request, res: Response) => {
  try {
    const ids = req.body.ids;
    await services.deleteBlog(ids);
    res.json({ message: `message.deletePost` });
  } catch (error) {
    res.json({ message: `${error}` });
  }
};


export default {
  getProducts,
  createProduct,
  updateProduct,
  deleteProduct,
  getById,
  bulkDltProduct,
};
