import { Router } from "express";
import Controller from "../controller/user-controller";

const controller = new Controller();
export const router: Router = Router();

router.post("/register", controller.register);
router.post("/logIn", controller.logIn);
