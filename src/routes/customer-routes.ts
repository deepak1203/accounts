import { Router } from "express";
import controller from "../controller/customer-controller";

const router: Router = Router();

router.get("/", controller.getCutomers);
router.post("/", controller.addCustomers);
router.delete("/:id", controller.deleteCustomer);

export default router;