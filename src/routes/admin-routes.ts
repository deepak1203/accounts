import { Router } from "express";
import Controller from "../controller/admin-controller";
// import { auth } from "../middleware/auth";

const controller = new Controller();
export const router: Router = Router();

router.post("/register", controller.register);
router.post("/logIn", controller.logIn);
