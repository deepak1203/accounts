import { Router } from "express";
import controller from "../controller/products-controller";
import multer from "multer";

export const router: Router = Router();
const upload = multer({ storage: multer.memoryStorage() });

router.get("/", controller.getProducts);
router.post("/", upload.single('productImage'), controller.createProduct);
router.get("/:id", controller.getById);
router.put("/:id", controller.updateProduct);
router.delete("/:id", controller.deleteProduct);
router.delete("/", controller.bulkDltProduct);
