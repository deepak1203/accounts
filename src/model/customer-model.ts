import sequelize from "sequelize";
import { sequelizeConnection as db } from "../utils/database";

export const Customers = db.define("customers", {
  id: {
    type: sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  firstName: {
    type: sequelize.STRING,
    allowNull: false,
  },
  lastName: {
    type: sequelize.STRING,
    allowNull: false,
  },
  address1: {
    type: sequelize.STRING,
    allowNull: false,
  },
  address2: {
    type: sequelize.STRING,
    allowNull: false,
  },
  city: {
    type: sequelize.STRING,
    allowNull: false,
  },
  state: {
    type: sequelize.STRING,
    allowNull: false,
  },
  zip: {
    type: sequelize.NUMBER,
    allowNull: false,
  },
  country: {
    type: sequelize.STRING,
    allowNull: false,
  },
});
