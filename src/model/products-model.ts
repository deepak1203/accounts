import { DataTypes, Model } from "sequelize";
import { sequelizeConnection as db } from "../utils/database";

export interface IProductAttributes {
  id?: number;
  title: string;
  details: string;
  price: number;
  imgURL: string;
}

export class Products extends Model<IProductAttributes> {
}

Products.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    details: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    imgURL: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  },
  {
    sequelize: db,
    tableName: "products",
  }
);
