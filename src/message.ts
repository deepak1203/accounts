export default {
  welcome: `Welcome you are successfully logged in...🙏🏻`,
  createPost: `The post is created successfully.`,
  addCutomer: `The customer is successfully added.`,
  updatePost: `Successfully updated.`,
  deletePost: `Successfully deleted.`,
  required: `All fields are required.`,
  login: `User already registered, please logIn.`,
  wrong: `Email id is wrong`,
  invalid: `Invalid password`,
  denied: `Access denied`,
  notFound: `Id not found, please enter a valid id.`,
  authError: `Sorry you are not authorised, please try again`
};
