import { Admin } from "../model/admin-model";
import Token from "../helper/token";

export const register = (fullName: string, email: string, password: string) => {
  return Admin.create({
    fullName: fullName,
    email: email,
    password: password,
  });
};

export const login = (admin: any) =>
  Token.createToken(admin.getDataValue("id"), admin.getDataValue("fullName"));
