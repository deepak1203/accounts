import { Customers } from "../model/customer-model";

const getAll = async() => await Customers.findAll();

const create = (
  firstName: string,
  lastName: string,
  address1: string,
  address2: string,
  city: string,
  state: string,
  zip: number,
  country: string
) => {
  return Customers.create({
    firstName,
    lastName,
    address1,
    address2,
    city,
    state,
    zip,
    country,
  });
};

const deleteCustomer = (id: number) =>
  Customers.destroy({ where: { id: id } });

export default {
  getAll,
  create,
  deleteCustomer,
};
