import { User } from "../model/user-model";
import Token from "../helper/token";

export const register = (
  firstName: string,
  lastName: string,
  email: string,
  password: string
) => {
  return User.create({
    firstName,
    lastName,
    email,
    password,
  });
};

export const login = (user: any) =>
  Token.createToken(user.getDataValue("id"), user.getDataValue("fullName"));
