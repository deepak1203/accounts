import { Products } from "../model/products-model";

const getAll = () => Products.findAll();

const getById = (id: number) => Products.findByPk(id);

const create = (title: string, details: string, image: string, price: number) => {  
  return Products.create({
    title: title,
    details: details,
    imgURL: image,
    price: price,
  });
};

const update = (id: number, title: string, details: string, price: number) => {
  return Products.update(
    {
      title,
      details,
      price,
    },
    { where: { id: id } }
  );
};

const deleteBlog = (id: number) => Products.destroy({ where: { id: id } });

const bulkDelete = (contentIds: any) => {
  return Products.destroy({ where: { id: contentIds} })
}

export default {
  getAll,
  create,
  update,
  deleteBlog,
  getById,
  bulkDelete,
};
