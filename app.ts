import express, { Application } from "express";
import { sequelizeConnection as db } from "./src/utils/database";
import { router as user } from "./src/routes/user-routes";
import { router as admin } from "./src/routes/admin-routes";
import { router as products } from "./src/routes/product-routes";
import customers from "./src/routes/customer-routes";
import config from "./src/config/config";
import { config as dotenv } from "dotenv";
import cors from "cors";

dotenv();
const app: Application = express();
const port = config.server.port;

db.sync({ alter: true });

app.use(express.json());
app.use(cors());
app.use(function (_req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use("/user", user);
app.use("/admin", admin);
app.use("/products", products);
app.use("/customers", customers);

app.listen(port, () => {
  console.log(`The server is running at http://${config.server.host}:${port}/`);
});
